# WorldFiller

This plugin fills in blocks below Y = 0 on plot and flat worlds. We used this plugin on our Creative server when we updated to 1.18. Essentially, this plugin grabs the blocks at Y = 0 or Y = 1 and stacks them down. It also removes the existing bedrock and generates a new bedrock layer at the lowest Y level.

**Before using this plugin, make a backup of your worlds**.

This plugin has been made for Paper. I don't know if it works with other server software.

It should be noted that Paper is terribly bad at loading tons of chunks fast. I don't know why, but you might experience insane CPU usage and insane memory usage while running this plugin. I can only advise starting Paper with enough memory and having patience as conversion may take a while. It might also help to convert your worlds to 1.18 first and then running this converter on the 1.18 worlds.

While running this converter, is has happened multiple times to me that console gets spammed with invalid block entity errors and blocks between certain Y levels are removed. Apparently this is a Multiverse issue, see [here](https://github.com/PaperMC/Paper/issues/6988). Abort, grab your world files from a backup, and restart the process if this happens.

The converter will convert all chunks in your region files that have some data. This means those chunks will all get generated. Maybe some of the surrounding chunks will get created by Paper as well. This can have a significant impact on disk usage. Especially for flat worlds, I'd advise running some kind of tool to delete region files with no player builds inside them before and after conversion.

You can run the converter using the command `/fillworld`. This should really only be done from the console, but there's also the permission `worldfiller.command` if you want to run it in-game. The usage is as follows:

1. Run `/fillworld start <world> <type>` with type set to "plot" or "flat". The plot type stacks blocks down, while the flat type fills with dirt. Edit the code if you wish to change the behaviour.
2. Run `/fillworld toggle <world>` to pause/unpause the converter in a world.
3. Run `/fillworld speed <max-loaded-chunks> <chunks-per-batch>` to change how fast the converter runs. If there are tons of garbage collections messages or if the server is hogging the CPU, you might want to lower these limits. Chunks per batch means how many chunks it tries to load in parallel.
4. Use `/fillworld stop <world>` to stop the converter in a world.
