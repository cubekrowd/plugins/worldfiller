package net.cubekrowd.worldfiller;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.plugin.Plugin;

public class FlatProcessor implements WorldScanner.ChunkProcessor {
    public void notifySkipped(Plugin plugin, Chunk chunk) {
        int chunkX = chunk.getX();
        int chunkZ = chunk.getZ();
        int posX = chunkX << 4;
        int posZ = chunkZ << 4;
        plugin.getLogger().info("Didn't fill entire chunk " + chunkX + ", " + chunkZ + " (at " + posX + " -64 " + posZ + ")");
    }

    @Override
    public void processChunk(WorldScanner scanner, Chunk chunk) {
        if (chunk.getBlock(0, chunk.getWorld().getMinHeight(), 0).getType() == Material.BEDROCK) {
            // @NOTE(traks) already processed in a previous run
            return;
        }

        int columnSkipCount = 0;
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                var floor = chunk.getBlock(x, 0, z);
                var floorType = floor.getType();
                if (floorType == Material.BEDROCK) {
                    var above = floor.getRelative(BlockFace.UP);
                    var aboveType = above.getType();
                    if (aboveType == Material.DIRT || aboveType == Material.GRASS) {
                        PlotProcessor.fill(chunk, x, z, Material.DIRT, true);
                    } else {
                        PlotProcessor.fill(chunk, x, z, Material.DIRT, false);
                    }
                } else {
                    if (PlotProcessor.isSolidBlock(floorType)) {
                        PlotProcessor.fill(chunk, x, z, Material.DIRT, false);
                    } else {
                        columnSkipCount++;
                    }
                }
            }
        }

        if (columnSkipCount > 0) {
            notifySkipped(scanner.plugin, chunk);
        }
    }
}
