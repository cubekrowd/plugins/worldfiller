package net.cubekrowd.worldfiller;

import com.plotsquared.core.PlotSquared;
import com.plotsquared.core.location.Location;
import com.plotsquared.core.plot.PlotId;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Blocks;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_18_R2.CraftChunk;
import org.bukkit.craftbukkit.v1_18_R2.util.CraftMagicNumbers;
import org.bukkit.plugin.Plugin;

public class PlotProcessor implements WorldScanner.ChunkProcessor {
    public Set<PlotId> partiallySkippedPlots = new HashSet<>();

    public static void fill(Chunk chunk, int x, int z, Material type, boolean replaceY0) {
        int end = replaceY0 ? 0 : -1;
        // chunk.getBlock(x, chunk.getWorld().getMinHeight(), z).setType(Material.BEDROCK, false);
        // for (int y = chunk.getWorld().getMinHeight() + 1; y <= end; y++) {
        //     chunk.getBlock(x, y, z).setType(type, false);
        // }

        // @NOTE(traks) use NMS to avoid going through the light engine. Without
        // NMS we spam the light engine with update, and it'll set up looooong
        // chains of completable futures that make server run out of memory.
        var nmsType = CraftMagicNumbers.getBlock(type).defaultBlockState();
        var nmsChunk = ((CraftChunk) chunk).getHandle();
        var blockPos = new BlockPos.MutableBlockPos();
        nmsChunk.setBlockState(blockPos.set(x, chunk.getWorld().getMinHeight(), z), Blocks.BEDROCK.defaultBlockState(), false, false);
        for (int y = chunk.getWorld().getMinHeight() + 1; y <= end; y++) {
            nmsChunk.setBlockState(blockPos.set(x, y, z), nmsType, false, false);
        }
    }

    public void notifySkipped(Plugin plugin, Chunk chunk, int offsetX, int offsetZ) {
        int chunkX = chunk.getX();
        int chunkZ = chunk.getZ();
        int posX = (chunkX << 4) + offsetX;
        int posZ = (chunkZ << 4) + offsetZ;
        var p2Loc = Location.at(chunk.getWorld().getName(), posX, 0, posZ);
        var plotArea = PlotSquared.get().getPlotAreaManager().getPlotArea(p2Loc);
        var plot = plotArea == null ? null : plotArea.getPlot(p2Loc);

        if (plot != null) {
            if (!partiallySkippedPlots.add(plot.getId())) {
                return;
            }

            plugin.getLogger().info(String.format(Locale.ENGLISH,
                    "Didn't fill entire plot %s (at %s %s %s)",
                    plot.getId(), posX, -64, posZ));
        } else {
            plugin.getLogger().info(String.format(Locale.ENGLISH,
                    "Didn't fill entire road chunk %s, %s (at %s %s %s)",
                    chunkX, chunkZ, posX, -64, posZ));
        }
    }

    public static boolean isSolidBlock(Material mat) {
        switch (mat) {
        case STONE:
        case GRANITE:
        case POLISHED_GRANITE:
        case DIORITE:
        case POLISHED_DIORITE:
        case ANDESITE:
        case POLISHED_ANDESITE:
        case DEEPSLATE:
        case COBBLED_DEEPSLATE:
        case POLISHED_DEEPSLATE:
        case CALCITE:
        case TUFF:
        case DRIPSTONE_BLOCK:
        case GRASS_BLOCK:
        case DIRT:
        case COARSE_DIRT:
        case PODZOL:
        case ROOTED_DIRT:
        case CRIMSON_NYLIUM:
        case WARPED_NYLIUM:
        case COBBLESTONE:
        case OAK_PLANKS:
        case SPRUCE_PLANKS:
        case BIRCH_PLANKS:
        case JUNGLE_PLANKS:
        case ACACIA_PLANKS:
        case DARK_OAK_PLANKS:
        case CRIMSON_PLANKS:
        case WARPED_PLANKS:
        case BEDROCK:
        case COAL_ORE:
        case DEEPSLATE_COAL_ORE:
        case IRON_ORE:
        case DEEPSLATE_IRON_ORE:
        case COPPER_ORE:
        case DEEPSLATE_COPPER_ORE:
        case GOLD_ORE:
        case DEEPSLATE_GOLD_ORE:
        case REDSTONE_ORE:
        case DEEPSLATE_REDSTONE_ORE:
        case EMERALD_ORE:
        case DEEPSLATE_EMERALD_ORE:
        case LAPIS_ORE:
        case DEEPSLATE_LAPIS_ORE:
        case DIAMOND_ORE:
        case DEEPSLATE_DIAMOND_ORE:
        case NETHER_GOLD_ORE:
        case NETHER_QUARTZ_ORE:
        case ANCIENT_DEBRIS:
        case COAL_BLOCK:
        case RAW_IRON_BLOCK:
        case RAW_COPPER_BLOCK:
        case RAW_GOLD_BLOCK:
        case AMETHYST_BLOCK:
        case BUDDING_AMETHYST:
        case IRON_BLOCK:
        case COPPER_BLOCK:
        case GOLD_BLOCK:
        case DIAMOND_BLOCK:
        case NETHERITE_BLOCK:
        case WAXED_COPPER_BLOCK:
        case WAXED_EXPOSED_COPPER:
        case WAXED_WEATHERED_COPPER:
        case WAXED_OXIDIZED_COPPER:
        case WAXED_CUT_COPPER:
        case WAXED_EXPOSED_CUT_COPPER:
        case WAXED_WEATHERED_CUT_COPPER:
        case WAXED_OXIDIZED_CUT_COPPER:
        case OAK_LOG:
        case SPRUCE_LOG:
        case BIRCH_LOG:
        case JUNGLE_LOG:
        case ACACIA_LOG:
        case DARK_OAK_LOG:
        case CRIMSON_STEM:
        case WARPED_STEM:
        case STRIPPED_OAK_LOG:
        case STRIPPED_SPRUCE_LOG:
        case STRIPPED_BIRCH_LOG:
        case STRIPPED_JUNGLE_LOG:
        case STRIPPED_ACACIA_LOG:
        case STRIPPED_DARK_OAK_LOG:
        case STRIPPED_CRIMSON_STEM:
        case STRIPPED_WARPED_STEM:
        case STRIPPED_OAK_WOOD:
        case STRIPPED_SPRUCE_WOOD:
        case STRIPPED_BIRCH_WOOD:
        case STRIPPED_JUNGLE_WOOD:
        case STRIPPED_ACACIA_WOOD:
        case STRIPPED_DARK_OAK_WOOD:
        case STRIPPED_CRIMSON_HYPHAE:
        case STRIPPED_WARPED_HYPHAE:
        case OAK_WOOD:
        case SPRUCE_WOOD:
        case BIRCH_WOOD:
        case JUNGLE_WOOD:
        case ACACIA_WOOD:
        case DARK_OAK_WOOD:
        case CRIMSON_HYPHAE:
        case WARPED_HYPHAE:
        case LAPIS_BLOCK:
        case SANDSTONE:
        case CHISELED_SANDSTONE:
        case CUT_SANDSTONE:
        case WHITE_WOOL:
        case ORANGE_WOOL:
        case MAGENTA_WOOL:
        case LIGHT_BLUE_WOOL:
        case YELLOW_WOOL:
        case LIME_WOOL:
        case PINK_WOOL:
        case GRAY_WOOL:
        case LIGHT_GRAY_WOOL:
        case CYAN_WOOL:
        case PURPLE_WOOL:
        case BLUE_WOOL:
        case BROWN_WOOL:
        case GREEN_WOOL:
        case RED_WOOL:
        case BLACK_WOOL:
        case MOSS_BLOCK:
        case SMOOTH_QUARTZ:
        case SMOOTH_RED_SANDSTONE:
        case SMOOTH_SANDSTONE:
        case SMOOTH_STONE:
        case BRICKS:
        case BOOKSHELF:
        case MOSSY_COBBLESTONE:
        case OBSIDIAN:
        case PURPUR_BLOCK:
        case PURPUR_PILLAR:
        case CRAFTING_TABLE:
        case SNOW_BLOCK:
        case CLAY:
        case NETHERRACK:
        case BASALT:
        case POLISHED_BASALT:
        case SMOOTH_BASALT:
        case GLOWSTONE:
        case STONE_BRICKS:
        case MOSSY_STONE_BRICKS:
        case CRACKED_STONE_BRICKS:
        case CHISELED_STONE_BRICKS:
        case DEEPSLATE_BRICKS:
        case CRACKED_DEEPSLATE_BRICKS:
        case DEEPSLATE_TILES:
        case CRACKED_DEEPSLATE_TILES:
        case CHISELED_DEEPSLATE:
        case MYCELIUM:
        case NETHER_BRICKS:
        case CRACKED_NETHER_BRICKS:
        case CHISELED_NETHER_BRICKS:
        case END_STONE:
        case END_STONE_BRICKS:
        case EMERALD_BLOCK:
        case CHISELED_QUARTZ_BLOCK:
        case QUARTZ_BLOCK:
        case QUARTZ_BRICKS:
        case QUARTZ_PILLAR:
        case WHITE_TERRACOTTA:
        case ORANGE_TERRACOTTA:
        case MAGENTA_TERRACOTTA:
        case LIGHT_BLUE_TERRACOTTA:
        case YELLOW_TERRACOTTA:
        case LIME_TERRACOTTA:
        case PINK_TERRACOTTA:
        case GRAY_TERRACOTTA:
        case LIGHT_GRAY_TERRACOTTA:
        case CYAN_TERRACOTTA:
        case PURPLE_TERRACOTTA:
        case BLUE_TERRACOTTA:
        case BROWN_TERRACOTTA:
        case GREEN_TERRACOTTA:
        case RED_TERRACOTTA:
        case BLACK_TERRACOTTA:
        case TERRACOTTA:
        case PRISMARINE:
        case PRISMARINE_BRICKS:
        case DARK_PRISMARINE:
        case SEA_LANTERN:
        case RED_SANDSTONE:
        case CHISELED_RED_SANDSTONE:
        case CUT_RED_SANDSTONE:
        case NETHER_WART_BLOCK:
        case WARPED_WART_BLOCK:
        case RED_NETHER_BRICKS:
        case BONE_BLOCK:
        case WHITE_GLAZED_TERRACOTTA:
        case ORANGE_GLAZED_TERRACOTTA:
        case MAGENTA_GLAZED_TERRACOTTA:
        case LIGHT_BLUE_GLAZED_TERRACOTTA:
        case YELLOW_GLAZED_TERRACOTTA:
        case LIME_GLAZED_TERRACOTTA:
        case PINK_GLAZED_TERRACOTTA:
        case GRAY_GLAZED_TERRACOTTA:
        case LIGHT_GRAY_GLAZED_TERRACOTTA:
        case CYAN_GLAZED_TERRACOTTA:
        case PURPLE_GLAZED_TERRACOTTA:
        case BLUE_GLAZED_TERRACOTTA:
        case BROWN_GLAZED_TERRACOTTA:
        case GREEN_GLAZED_TERRACOTTA:
        case RED_GLAZED_TERRACOTTA:
        case BLACK_GLAZED_TERRACOTTA:
        case WHITE_CONCRETE:
        case ORANGE_CONCRETE:
        case MAGENTA_CONCRETE:
        case LIGHT_BLUE_CONCRETE:
        case YELLOW_CONCRETE:
        case LIME_CONCRETE:
        case PINK_CONCRETE:
        case GRAY_CONCRETE:
        case LIGHT_GRAY_CONCRETE:
        case CYAN_CONCRETE:
        case PURPLE_CONCRETE:
        case BLUE_CONCRETE:
        case BROWN_CONCRETE:
        case GREEN_CONCRETE:
        case RED_CONCRETE:
        case BLACK_CONCRETE:
        case REDSTONE_BLOCK:
        case SHROOMLIGHT:
        case HONEYCOMB_BLOCK:
        case CRYING_OBSIDIAN:
        case BLACKSTONE:
        case GILDED_BLACKSTONE:
        case POLISHED_BLACKSTONE:
        case CHISELED_POLISHED_BLACKSTONE:
        case POLISHED_BLACKSTONE_BRICKS:
        case CRACKED_POLISHED_BLACKSTONE_BRICKS:
            return true;
        default:
            return false;
        }
    }

    @Override
    public void processChunk(WorldScanner scanner, Chunk chunk) {
        if (chunk.getBlock(0, chunk.getWorld().getMinHeight(), 0).getType() == Material.BEDROCK) {
            // @NOTE(traks) already processed in a previous run
            return;
        }

        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                var floor = chunk.getBlock(x, 0, z);
                var floorType = floor.getType();
                if (floorType == Material.BEDROCK) {
                    var above = floor.getRelative(BlockFace.UP);
                    var aboveType = above.getType();
                    if (isSolidBlock(aboveType)) {
                        fill(chunk, x, z, aboveType, true);
                    } else {
                        // @NOTE(traks) don't fill in any other case. E.g. if
                        // players build dark chambers above the void we don't
                        // want to place an ugly sandstone wall at the bottom of
                        // their chamber.
                        //
                        // -- Nevermind, do fill below anyway, this doesn't help
                        // that much, and prevents tons of blocks from being
                        // filled (e.g. if the plot's main block is not
                        // sandstone).
                        fill(chunk, x, z, scanner.defaultPlotFillBlock, false);
                    }
                } else {
                    if (isSolidBlock(floorType)) {
                        fill(chunk, x, z, floorType, false);
                    } else {
                        notifySkipped(scanner.plugin, chunk, x, z);
                    }
                }
            }
        }
    }
}
