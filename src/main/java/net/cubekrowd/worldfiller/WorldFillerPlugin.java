package net.cubekrowd.worldfiller;

import java.util.ArrayList;
import java.util.Locale;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class WorldFillerPlugin extends JavaPlugin {
    @Override
    public void onDisable() {
        for (var scanner : new ArrayList<>(WorldScanner.runningScanners)) {
            scanner.stop();
        }
    }

    public static String getArg(String[] args, int index) {
        if (args.length <= index) {
            return "";
        }
        return args[index].toLowerCase(Locale.ENGLISH);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        var sub = getArg(args, 0);
        if (sub.equals("start")) {
            var worldName = getArg(args, 1);
            var world = Bukkit.getWorld(worldName);
            if (world == null) {
                sender.sendMessage(String.format("Unknown world %s", worldName));
            } else {
                var existingScanner = WorldScanner.runningScanners.stream()
                        .filter(s -> s.world.equals(world))
                        .findAny();
                if (existingScanner.isPresent()) {
                    sender.sendMessage("That world is already being filled");
                    return true;
                }

                var type = getArg(args, 2);

                var startRegionFile = getArg(args, 3);
                startRegionFile = startRegionFile.isEmpty() ? null : startRegionFile;

                if (type.equals("plot")) {
                    sender.sendMessage(String.format("Running a scanner in world: %s", world.getName()));
                    var scanner = new WorldScanner(this, world);
                    scanner.processor = new PlotProcessor();
                    scanner.startRegionFileName = startRegionFile;
                    // @TODO(traks) make this configurable?
                    scanner.defaultPlotFillBlock = Material.COAL_BLOCK;
                    scanner.start();
                } else if (type.equals("flat")) {
                    sender.sendMessage(String.format("Running a scanner in world: %s", world.getName()));
                    var scanner = new WorldScanner(this, world);
                    scanner.processor = new FlatProcessor();
                    scanner.startRegionFileName = startRegionFile;
                    scanner.start();
                } else {
                    sender.sendMessage(String.format("Unknown type %s", type));
                }
            }
        } else if (sub.equals("stop")) {
            var worldName = getArg(args, 1);
            var world = Bukkit.getWorld(worldName);
            if (world == null) {
                sender.sendMessage(String.format("Unknown world %s", worldName));
            } else {
                var existingScanner = WorldScanner.runningScanners.stream()
                        .filter(s -> s.world.equals(world))
                        .findAny();
                if (existingScanner.isPresent()) {
                    existingScanner.get().stop();
                    sender.sendMessage(String.format("Stopped the scanner in world: %s", world.getName()));
                } else {
                    sender.sendMessage("That world is not being filled");
                }
            }
        } else if (sub.equals("toggle")) {
            var worldName = getArg(args, 1);
            var world = Bukkit.getWorld(worldName);
            if (world == null) {
                sender.sendMessage(String.format("Unknown world %s", worldName));
            } else {
                var existingScanner = WorldScanner.runningScanners.stream()
                        .filter(s -> s.world.equals(world))
                        .findAny();
                if (existingScanner.isPresent()) {
                    var paused = existingScanner.get().paused;
                    existingScanner.get().paused = !paused;
                    if (paused) {
                        sender.sendMessage(String.format("Unpaused the scanner in world: %s", world.getName()));
                    } else {
                        sender.sendMessage(String.format("Paused the scanner in world: %s", world.getName()));
                    }
                } else {
                    sender.sendMessage("That world is not being filled");
                }
            }
        } else if (sub.equals("speed")) {
            int maxLoadedChunks = WorldScanner.maxLoadedChunks;
            int chunksPerBatch = WorldScanner.chunksPerBatch;
            try {
                maxLoadedChunks = Integer.parseInt(getArg(args, 1));
                chunksPerBatch = Integer.parseInt(getArg(args, 2));
            } catch (NumberFormatException e) {
                // @NOTE(traks) ignore
            }
            WorldScanner.maxLoadedChunks = maxLoadedChunks;
            WorldScanner.chunksPerBatch = chunksPerBatch;
            sender.sendMessage(String.format("New speed: %s max loaded chunks, %s chunks per batch", maxLoadedChunks, chunksPerBatch));
        } else {
            sender.sendMessage("Usage: /fillworld <start/stop> <world> [type]");
        }
        return true;
    }
}
