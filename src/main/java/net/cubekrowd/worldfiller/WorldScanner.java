package net.cubekrowd.worldfiller;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_18_R2.CraftWorld;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

public class WorldScanner implements Listener {
    public static final Pattern REGION_FILE_PATTERN = Pattern.compile("r\\.(-?[0-9]+).(-?[0-9]+)\\.mca");
    public static final int NOTIFICATION_INTERVAL_SECONDS = 20;

    public static int maxLoadedChunks = 2000;
    public static int chunksPerBatch = 64;
    public static List<WorldScanner> runningScanners = new ArrayList<>();

    public Plugin plugin;
    public World world;
    public ChunkProcessor processor;
    public boolean paused;
    public String startRegionFileName;
    public Material defaultPlotFillBlock;

    public boolean initialised;
    public List<Path> regionFiles;
    public int regionFileIndex;
    public int curChunkIndex;
    public int curRegionX;
    public int curRegionZ;
    public AtomicInteger asyncLoadScheduled = new AtomicInteger();
    public boolean running;
    public boolean shouldStop;
    public List<Chunk> loadedChunks;

    public int processedChunkCount;
    public long lastNotificationNanos;

    public boolean reachedMemoryLimit;
    public long reachedMemoryStartNanos;

    public static class FailedChunk {
        public int failCount;
        public long lastFailTime;
        public boolean resubmitted;

        @Override
        public String toString() {
            return "FailedChunk{" +
                    "failCount=" + failCount +
                    ", lastFailTime=" + lastFailTime +
                    ", resubmitted=" + resubmitted +
                    '}';
        }
    }
    public static record ChunkPos(int x, int z) {}

    public Map<ChunkPos, FailedChunk> tempFailedChunks;
    public int permFailedChunkCount;
    public int nonExistentChunkCount;

    public interface ChunkProcessor {
        void processChunk(WorldScanner scanner, Chunk chunk);
    }

    public WorldScanner(Plugin plugin, World world) {
        this.plugin = plugin;
        this.world = world;
    }

    public void start() {
        if (running) {
            return;
        }
        plugin.getLogger().info("Started scanning the world: " + world.getName());
        Bukkit.getPluginManager().registerEvents(this, plugin);
        initialised = false;
        paused = false;
        running = true;
        shouldStop = false;
        runningScanners.add(this);
    }

    public void stop() {
        plugin.getLogger().info("Stopped scanning the world: " + world.getName());
        HandlerList.unregisterAll(this);
        regionFiles = null;
        running = false;

        for (var chunk : loadedChunks) {
            chunk.removePluginChunkTicket(plugin);
            world.unloadChunkRequest(chunk.getX(), chunk.getZ());
        }
        loadedChunks = null;

        tempFailedChunks = null;

        runningScanners.remove(this);
    }

    public void handleFailedChunk(int chunkX, int chunkZ, Throwable t) {
        int posX = chunkX << 4;
        int posZ = chunkZ << 4;

        var chunkPos = new ChunkPos(chunkX, chunkZ);
        var failedChunk = tempFailedChunks.computeIfAbsent(chunkPos, k -> new FailedChunk());
        failedChunk.failCount++;
        failedChunk.lastFailTime = System.nanoTime();
        failedChunk.resubmitted = false;

        if (failedChunk.failCount > 5) {
            plugin.getLogger().log(Level.WARNING, String.format(
                    "Failed to load chunk %s, %s (at %s %s %s)",
                    chunkX, chunkZ, posX, -64, posZ), t);
            tempFailedChunks.remove(chunkPos);
            permFailedChunkCount++;
        }
    }

    // @NOTE(traks) based on loadChunk in CraftWorld
    public boolean chunkExists(int x, int z) {
        var nmsWorld = ((CraftWorld) world).getHandle();
        net.minecraft.world.level.ChunkPos chunkPos = new net.minecraft.world.level.ChunkPos(x, z);
        try {
            return nmsWorld.getChunkSource().chunkMap.regionFileCache.chunkExists(chunkPos);
        } catch (IOException e) {
            return true;
        }
    }

    public void requestChunk(int chunkX, int chunkZ) {
        // @NOTE(traks) we need to generate chunks, because the blocks
        // could've been generated while the entire chunk isn't finished
        // yet, which means we don't upgrade the chunk correctly even though
        // we should.

        // @NOTE(traks) for some reason this is all very slow. Takes
        // approximately 2 seconds on an SSD to process 1 region file of a flat
        // world. WTF? Should be at least 10x faster.

        if (!chunkExists(chunkX, chunkZ)) {
            nonExistentChunkCount++;
            return;
        }

        asyncLoadScheduled.incrementAndGet();

        var future = world.getChunkAtAsync(chunkX, chunkZ, true);
        future.whenComplete((chunk, t) -> {
            if (!running) {
                return;
            }

            asyncLoadScheduled.decrementAndGet();
            if (t != null) {
                handleFailedChunk(chunkX, chunkZ, t);
            } else {
                if (chunk == null) {
                    handleFailedChunk(chunkX, chunkZ, new NullPointerException("Chunk is null"));
                    return;
                }
                try {
                    chunk.addPluginChunkTicket(plugin);
                } catch (Exception ex) {
                    handleFailedChunk(chunkX, chunkZ, ex);
                    return;
                }

                loadedChunks.add(chunk);
                tempFailedChunks.remove(new ChunkPos(chunkX, chunkZ));
            }
        });
    }

    public void requestBatch() {
        if (Runtime.getRuntime().freeMemory() < 50 * 1024 * 1024) {
            if (!reachedMemoryLimit) {
                plugin.getLogger().info("Reached memory limit, waiting for memory to free up");
                reachedMemoryLimit = true;
                reachedMemoryStartNanos = System.nanoTime();
            }
        }

        if (reachedMemoryLimit) {
            if (System.nanoTime() - reachedMemoryStartNanos > (long) 4 * 1_000_000_000) {
                reachedMemoryStartNanos = System.nanoTime();
                plugin.getLogger().info("Trying to force garbage collection");
                System.gc();

                if (Runtime.getRuntime().freeMemory() > 200 * 1024 * 1024) {
                    reachedMemoryLimit = false;
                }
            }
            return;
        }

        if (curChunkIndex == 0) {
            // @NOTE(traks) before working on a new region file, try to load all
            // the chunks that failed to load. This can happen if a neighbour of
            // a chunk we're trying to load is being unloaded... urg...
            if (!tempFailedChunks.isEmpty()) {
                var tempFailedCopy = new HashMap<>(tempFailedChunks);
                int batchIndex = 0;
                for (var entry : tempFailedCopy.entrySet()) {
                    var chunkPos = entry.getKey();
                    var failedChunk = entry.getValue();

                    if (System.nanoTime() - failedChunk.lastFailTime > (long) 10 * 1_000_000_000) {
                        if (failedChunk.resubmitted) {
                            // @NOTE(traks) chunk has already been requested for
                            // loading, so don't send another request!
                            continue;
                        }

                        failedChunk.resubmitted = true;
                        requestChunk(chunkPos.x, chunkPos.z);
                        batchIndex++;
                        if (batchIndex >= chunksPerBatch) {
                            break;
                        }
                    }
                }
                return;
            }

            int totalChunksSeen = processedChunkCount + permFailedChunkCount + tempFailedChunks.size() + loadedChunks.size() + asyncLoadScheduled.get() + nonExistentChunkCount;
            if (regionFileIndex * 1024 != totalChunksSeen) {
                pushProgressNotification();
                plugin.getLogger().severe(String.format(
                        "Processed %s region files, but only saw %s chunks in total! Missed some chunks, so stopping",
                        regionFileIndex, totalChunksSeen));
                shouldStop = true;
                return;
            }

            if (regionFileIndex >= regionFiles.size()) {
                if (processedChunkCount + nonExistentChunkCount == totalChunksSeen) {
                    // @NOTE(traks) done!
                    plugin.getLogger().log(Level.INFO, "Finished processing");
                    shouldStop = true;
                }
                return;
            }

            // @TODO(traks) should move this to preprocessing stage, since if a
            // file is not a region file, the above will count it as a region
            // file and thus calculate the incorrect number of chunks.

            var path = regionFiles.get(regionFileIndex);
            var name = path.getFileName().toString();

            plugin.getLogger().info(String.format(Locale.ENGLISH,
                    "Starting work on region file %s (%s/%s, %.2f%%)",
                    name,
                    regionFileIndex + 1, regionFiles.size(),
                    (double) (regionFileIndex + 1) / regionFiles.size() * 100));

            var matcher = REGION_FILE_PATTERN.matcher(name);
            if (!matcher.find()) {
                plugin.getLogger().warning("Found a file that isn't a region file: " + name);
                regionFileIndex++;
                return;
            }
            var stringX = matcher.group(1);
            var stringZ = matcher.group(2);
            try {
                curRegionX = Integer.parseInt(stringX);
                curRegionZ = Integer.parseInt(stringZ);
            } catch (NumberFormatException e) {
                plugin.getLogger().warning("Encountered invalid x- or z-coordinate in " + name);
                regionFileIndex++;
                return;
            }
        }

        for (int i = 0; i < chunksPerBatch; i++) {
            if (curChunkIndex >= 1024) {
                curChunkIndex = 0;
                regionFileIndex++;
                break;
            }

            int chunkIndex = curChunkIndex;
            curChunkIndex++;

            int chunkX = (curRegionX << 5) + (chunkIndex & 0x1f);
            int chunkZ = (curRegionZ << 5) + (chunkIndex >> 5);
            requestChunk(chunkX, chunkZ);
        }
    }

    public void pushProgressNotification() {
        lastNotificationNanos = System.nanoTime();
        plugin.getLogger().info(String.format(
                "Chunks processed: %s, temp failed: %s, perm failed: %s, non-exist: %s, loaded: %s, background: %s. Free memory: %smb",
                processedChunkCount, tempFailedChunks.size(), permFailedChunkCount, nonExistentChunkCount, loadedChunks.size(), asyncLoadScheduled.get(),
                Runtime.getRuntime().freeMemory() / 1024 / 1024));
    }

    @EventHandler
    public void onTick(ServerTickEndEvent e) {
        if (!initialised) {
            initialised = true;

            loadedChunks = new ArrayList<>();
            regionFileIndex = 0;
            asyncLoadScheduled.set(0);
            processedChunkCount = 0;
            permFailedChunkCount = 0;
            tempFailedChunks = new HashMap<>();
            nonExistentChunkCount = 0;

            var regionFolder = new File(world.getWorldFolder(), "region").toPath();
            try (var regionFilesStream = Files.list(regionFolder)) {
                regionFiles = regionFilesStream.collect(Collectors.toList());
            } catch (Exception ex) {
                plugin.getLogger().log(Level.SEVERE, "Failed to list region files", ex);
                regionFiles = new ArrayList<>();
            }

            // @NOTE(traks) consistent ordering across runs so we can skip
            // regions if necessary after a reboot
            regionFiles.sort(Comparator.comparing(p -> p.getFileName().toString()));

            if (startRegionFileName != null) {
                var fileIter = regionFiles.iterator();
                while (fileIter.hasNext()) {
                    if (startRegionFileName.equals(fileIter.next().getFileName().toString())) {
                        break;
                    }

                    fileIter.remove();
                }

                plugin.getLogger().info(String.format("Starting at region file: %s", startRegionFileName));
            }
        }

        if (shouldStop) {
            stop();
            return;
        }

        long tickDurationMillis = (long) e.getTickDuration();
        long startNanos = System.nanoTime();
        long endNanos = (48 - tickDurationMillis) * 1_000_000 + startNanos;

        // @NOTE(traks) process chunks
        var chunkIter = loadedChunks.iterator();
        while (chunkIter.hasNext()) {
            var chunk = chunkIter.next();

            try {
                processor.processChunk(this, chunk);
            } catch (Exception ex) {
                plugin.getLogger().log(Level.WARNING, "Error while processing chunk " + chunk.getX() + ", " + chunk.getZ(), ex);
            } finally {
                processedChunkCount++;
                chunk.removePluginChunkTicket(plugin);
                chunkIter.remove();
                world.unloadChunkRequest(chunk.getX(), chunk.getZ());
            }

            if (System.nanoTime() > endNanos) {
                break;
            }
        }

        // @NOTE(traks) finish processing loaded chunks to clear them from
        // memory, but don't request any new batches
        if (paused) {
            return;
        }

        // @NOTE(traks) request new chunks
        int chunksInFlight = loadedChunks.size() + asyncLoadScheduled.get();
        int maxNewChunkCount = maxLoadedChunks - chunksInFlight;
        if (maxNewChunkCount / chunksPerBatch > 0) {
            requestBatch();
        }

        if (System.nanoTime() - lastNotificationNanos > NOTIFICATION_INTERVAL_SECONDS * (long) 1_000_000_000) {
            pushProgressNotification();
        }
    }
}
